"strict mode";

PREV_CLASS = ".prevButton";
NEXT_CLASS = ".nextButton";
SLIDEBOX_CLASS = ".slideBox";

class Carousel {
  constructor(data) {
    this.data = data;
    this.pos = 0;
    this.slideN = 3;
    this.render();
  }

  render() {
    // Assinging main carousel elements
    const prevButton = document.querySelector(PREV_CLASS);
    const nextButton = document.querySelector(NEXT_CLASS);
    const slideBox = document.querySelector(SLIDEBOX_CLASS);
    // Adding event listeners
    prevButton.addEventListener("click", () => this.moveSlide("prev"));
    nextButton.addEventListener("click", () => this.moveSlide("next"));

    // Change class name for all object with the same class

    // const changeClass = function (className, substitute) {
    //   const items = document.querySelectorAll(className);
    //   items.forEach((item) => {
    //     item.className = substitute;
    //   });
    // };

    // Creating slides
    for (let i = 0; i < this.data.length; i++) {
      // Promise loop

      new Promise(function (resolve) {
        const html = document.createElement("img");
        html.className = "slide";
        html.src = `https://picsum.photos/200/300?random=${i}`;
        slideBox.insertAdjacentElement("beforeend", html);
      }).then(() => resolve());

      // Default loop

      // const html = document.createElement("img");
      // html.className = "slide";
      // console.log(i);
      // html.src = `https://picsum.photos/200/300?random=${i}`;
      // html.addEventListener("load", () =>
      //   slideBox.insertAdjacentElement("beforeend", html)
      // );
    }
  }
  // Changing slide position depending on the direction assigned

  moveSlide(dir) {
    const el = document.querySelectorAll(".slide");
    if (dir === "next" && this.pos === this.data.length - this.slideN) {
      this.pos = 0;

      el.forEach((i) => {
        i.style.left = 0;
      });
    } else if (dir === "prev" && this.pos === 0) {
      this.pos = this.data.length - this.slideN;

      el.forEach((i) => {
        i.style.left = (this.data.length - this.slideN) * -200 + `px`;
      });
    }
    // When moving forward
    else if (dir === "next" && this.pos < this.data.length - this.slideN) {
      el.forEach((i) => {
        const val = Number(i.style.left.replace(/left:|px/, ""));
        i.style.left = val - 200 + `px`;
      });

      this.pos++;
      // When moving backforward
    } else if (dir === "prev" && this.pos > 0) {
      el.forEach((i) => {
        const val = Number(i.style.left.replace(/left:|px/, ""));
        i.style.left = val + 200 + `px`;
      });

      this.pos--;
    }
  }
}
