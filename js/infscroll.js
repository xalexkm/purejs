"strict mode";

let loadIterations = 1;
let isFetching = false;

const button = document.querySelector(".open-button");
const block = document.querySelector(".content");

button.addEventListener("click", () => fetchData());
document.addEventListener(
  "scroll",
  () => {
    infScroll();
  },
  false
);

// Fetching heap of the data from the beginning and then displaying on demand

// const loadData = (data) => {
//   for (let i = 0; i < 3; i++) {
//     if (data[i] === undefined) {
//       data.splice(0, 3);
//       return;
//     }
//     const div = document.createElement("div");
//     div.innerText = `${data[i].name}`;
//     div.className = "item";
//     block.appendChild(div);
//   }
//   data.splice(0, 3);
// };

// let fetchingData = fetch(`https://api.thedogapi.com/v1/breeds?limit=15`)
//   .then((res) => res.json())
//   .then((data) => {
//     for (let i = 0; i < 3; i++) {
//       const div = document.createElement("div");
//       div.innerText = `${data[i].name}`;
//       div.className = "item";
//       block.appendChild(div);
//     }
//     data.splice(0, 3);
//     button.addEventListener("click", () => loadData(data));
//   });

// Fetching data incrementally

async function fetchData() {
  if (isFetching) return;
  isFetching = true;
  const button = document.querySelector(".open-button");
  button.innerText = "";
  const loader = document.createElement("div");
  loader.className = "loader";
  button.appendChild(loader);
  await fetch(
    `https://api.thedogapi.com/v1/breeds?limit=10&page=${loadIterations}`
  )
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      console.log(data);
      if (data === []) {
        const button = document.querySelector(".open-button");
        button.remove();
      }
      for (let i = 0; i < 10; i++) {
        const div = document.createElement("div");
        div.innerText = `${data[i].name}`;
        div.className = "item";
        block.appendChild(div);
      }
    })
    .finally(() => {
      const button = document.querySelector(".open-button");
      const loader = document.querySelector(".loader");
      loader.remove();
      button.innerText = "Load more...";
    })
    .catch((err) => {
      console.log(err);
      return;
    });
  loadIterations++;
  isFetching = false;
  setTimeout;
  return;
}

async function infScroll() {
  if (
    window.innerHeight >=
      block.offsetHeight + block.offsetTop + 50 - window.scrollY &&
    !isFetching
  ) {
    fetchData();
  }
}

// ?api_key=0e786acc-5dfa-475a-bd79-8ea9e0c16e16
